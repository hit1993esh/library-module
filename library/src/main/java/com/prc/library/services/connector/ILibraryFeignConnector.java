package com.prc.library.services.connector;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.prc.library.model.Book;


@FeignClient("book-store")
public interface ILibraryFeignConnector {
	@RequestMapping(method = RequestMethod.PUT, value = "/books")
    Book updateBook(Book book);
}
