package com.prc.library.services;

import java.util.List;

import com.prc.library.model.Book;

public interface ILibraryService {

	public List<Book> getAllBooks() ;

	public Book getBookById(String bookId);

	public void addBook(Book book);

	public void deleteBookById(String bookId);

	public Book updateBook(Book book);

}
