package com.prc.library.services;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.prc.library.model.Book;
import com.prc.library.services.connector.ILibraryFeignConnector;

@Service
public class LibraryServiceImpl implements ILibraryService {
	
	@Autowired
	DiscoveryClient discoveryClient;
	Logger logger = LoggerFactory.getLogger(LibraryServiceImpl.class);
	
	@Autowired
	ILibraryFeignConnector iLibraryFeignConnector;
	
	@Override
	public List<Book> getAllBooks() {
		
		// Direct Remote Call
		
		RestTemplate restTemplate = new RestTemplate();
		return Arrays.asList(restTemplate
				.getForObject("http://localhost:8008/books/", Book[].class));
	}

	@Override
	public Book getBookById(String bookId) {
		
		// Direct Remote Call
		
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate
				.getForObject("http://localhost:8008/books/" + bookId, Book.class);
	}

	@Override
	public void addBook(Book book) {
		// Call via Service Discovery Eureka client
		RestTemplate restTemplate = new RestTemplate();
		logger.debug(restTemplate
				.postForObject(discoveryClient.
						getInstances("book-store").get(0).getUri() + "/books"
						, book, String.class));
	}

	@Override
	public void deleteBookById(String bookId) {
		// Call via API Gateway (zuul)
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(discoveryClient.
						getInstances("library-zuul-service").get(0).getUri() 
						+ "/book-api/books/" + bookId, String.class);
	}
	
	@Override
	public Book updateBook(Book book) {
		
		// Call using feign client
		
		return iLibraryFeignConnector.updateBook(book);
	}
	
}
