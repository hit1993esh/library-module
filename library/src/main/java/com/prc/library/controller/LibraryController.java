package com.prc.library.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prc.library.model.Book;
import com.prc.library.services.ILibraryService;

@RestController
@RequestMapping("/library")
public class LibraryController {
	
	@Autowired
	ILibraryService service;
	
	

	@RequestMapping(value = "/books", method = RequestMethod.GET)
	public List<Book> getAllBooks() {
		return service.getAllBooks();
	}

	@RequestMapping(value = "books/{bookId}", method = RequestMethod.GET)
	public Book getBookById(@PathVariable("bookId") String bookId) {
		return service.getBookById(bookId);
	}

	@RequestMapping(value = "books", method = RequestMethod.POST)
	public void addBook(@RequestBody Book book) {
		service.addBook(book);
	}

	@RequestMapping(value = "books/{bookId}", method = RequestMethod.DELETE)
	public void deleteBook(@PathVariable("bookId") String bookId) {
		service.deleteBookById(bookId);
	}
	
	@RequestMapping(value = "books", method = RequestMethod.PUT)
	public Book updateBook(@RequestBody Book book) {
		return service.updateBook(book);
	}

}
