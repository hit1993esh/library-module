package com.prc.bookStore.Dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prc.bookStore.models.Book;

public interface IBookDao extends JpaRepository<Book, String>{
		Book findByBookId(String bookId);
}
