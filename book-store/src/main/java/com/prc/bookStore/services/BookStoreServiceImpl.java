package com.prc.bookStore.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prc.bookStore.Dao.IBookDao;
import com.prc.bookStore.models.Book;

@Service
public class BookStoreServiceImpl implements IBookStoreService {

	@Autowired
	IBookDao dao;

	@Override
	public List<Book> getBookList() {
		return dao.findAll();
	}

	@Override
	public Book getById(String bookId) {
		return dao.findByBookId(bookId);
	}

	@Override
	public void add(Book book) {
		dao.save(book);
	}

	@Override
	public void deleteById(String bookId) {
		Book book = getById(bookId);
		dao.delete(book);
	}

	@Override
	public Book update(Book book) {
		return dao.save(book);
	}

}
