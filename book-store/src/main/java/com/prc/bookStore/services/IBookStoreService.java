package com.prc.bookStore.services;

import java.util.List;

import com.prc.bookStore.models.Book;

public interface IBookStoreService {

	public List<Book> getBookList() ;

	public Book getById(String bookId);

	public void add(Book book);

	public void deleteById(String bookId);

	public Book update(Book book);
	
}
