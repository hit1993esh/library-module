package com.prc.bookStore.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prc.bookStore.models.Book;
import com.prc.bookStore.services.IBookStoreService;

@RestController
@RequestMapping("/books")
public class BookStoreController {

	IBookStoreService service;
	
	public BookStoreController(IBookStoreService service) {
		this.service = service;
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Book> getBooks() {
		return service.getBookList();
	}

	@RequestMapping(value = "/{bookId}", method = RequestMethod.GET)
	public Book getBookById(@PathVariable("bookId") String bookId) {
		return service.getById(bookId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public void addBook(@RequestBody Book book) {
		service.add(book);
	}

	@RequestMapping(value = "/{bookId}", method = RequestMethod.DELETE)
	public void deleteBook(@PathVariable("bookId") String bookId) {

		service.deleteById(bookId);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public Book updateBook(@RequestBody Book book) {
		return service.update(book);
	}
}
