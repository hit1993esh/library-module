package com.prc.bookStore.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "BOOK")

@DynamicInsert
@DynamicUpdate
public class Book implements Serializable{
	
	private final static long serialVersionUID = 124654345L;
	
	@Id
	@Column(length = 50)
	private String bookId;

	@Column(length = 100)
	private String bookName;

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

}
